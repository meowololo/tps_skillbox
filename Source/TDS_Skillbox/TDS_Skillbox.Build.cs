// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TDS_Skillbox : ModuleRules
{
	public TDS_Skillbox(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule", "PhysicsCore", "Slate" });
    }
}
