// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_SkillboxCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "TDS_Skillbox/FunctionLibrary/GameTypesClass.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "TDS_Skillbox/Game/TDS_SkillboxGameInstance.h"


ATDS_SkillboxCharacter::ATDS_SkillboxCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->RotationRate = FRotator(0.f, 350.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

}

void ATDS_SkillboxCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	InitWeapon();
	
}

void ATDS_SkillboxCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	 if (StaminaCurrent > StaminaMin)
	 {
		 CanSprint = true;
	 }
	 else
	 {
		 CanSprint = false;
	 }
	
	MovementTick();

	MouseOnDisplayTick(DeltaSeconds);
	
}

void ATDS_SkillboxCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis("MoveForward", this, &ATDS_SkillboxCharacter::InputAxisX);
	NewInputComponent->BindAxis("MoveRight", this, &ATDS_SkillboxCharacter::InputAxisY);
	NewInputComponent->BindAction("SprintAction", IE_Pressed, this, &ATDS_SkillboxCharacter::InputActionStartSprint);
	NewInputComponent->BindAction("SprintAction", IE_Released, this, &ATDS_SkillboxCharacter::InputActionStopSprint);
	NewInputComponent->BindAction("FireAction", IE_Pressed, this, &ATDS_SkillboxCharacter::ShootAction);
	NewInputComponent->BindAction("FireAction", IE_Released, this, &ATDS_SkillboxCharacter::StopShootAction);
	NewInputComponent->BindAction("AimAction", IE_Pressed, this, &ATDS_SkillboxCharacter::EnableAim);
	NewInputComponent->BindAction("AimAction", IE_Released, this, &ATDS_SkillboxCharacter::DisableAim);
	NewInputComponent->BindAction("WalkAction", IE_Pressed, this, &ATDS_SkillboxCharacter::EnableWalk);
	NewInputComponent->BindAction("WalkAction", IE_Released, this, &ATDS_SkillboxCharacter::DisableWalk);
	NewInputComponent->BindAction("ReloadAction", IE_Released, this, &ATDS_SkillboxCharacter::ReloadAction);
	
}

void ATDS_SkillboxCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATDS_SkillboxCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATDS_SkillboxCharacter::InputActionStartSprint()
{
	if (StaminaCurrent > StaminaMin)
	{
		bInputSprint = true;
		ChangeMovementState();

		if (StaminaResTimer.IsValid())
		{
			GetWorldTimerManager().ClearTimer(StaminaResTimer);
		}

		GetWorldTimerManager().SetTimer(StaminaConsTimer, this, &ATDS_SkillboxCharacter::StaminaConsumption, StaminaConsRate, true);
	}

}

void ATDS_SkillboxCharacter::InputActionStopSprint()
{
		bInputSprint = false;
		ChangeMovementState();

		if (StaminaConsTimer.IsValid())
		{
			GetWorldTimerManager().ClearTimer(StaminaConsTimer);
		}

		GetWorldTimerManager().SetTimer(StaminaResTimer, this, &ATDS_SkillboxCharacter::StaminaRestoration, StaminaResRate, true, StaminaResDelay);
}

void ATDS_SkillboxCharacter::StaminaConsumption()
{
	if (CharIsMoving)
	{
		if (StaminaCurrent > StaminaMin)
		{
			StaminaCurrent--;
		}
		else
		{
			bInputSprint = false;
			ChangeMovementState();
			GetWorldTimerManager().PauseTimer(StaminaConsTimer);
		}
	}
}

void ATDS_SkillboxCharacter::StaminaRestoration()
{

	if (StaminaCurrent < StaminaMax)
	{
		StaminaCurrent++;
	}
	else
	{
		GetWorldTimerManager().PauseTimer(StaminaResTimer);
	}
}

void ATDS_SkillboxCharacter::EnableAim()
{
	AimEnabled = true;
	GEngine->AddOnScreenDebugMessage(-1, 0.5, FColor::Green, "AIM Enable");
	ChangeMovementState();
}

void ATDS_SkillboxCharacter::DisableAim()
{
	AimEnabled = false;
	GEngine->AddOnScreenDebugMessage(-1, 0.5, FColor::Green, "AIM Disable");
	ChangeMovementState();
}

void ATDS_SkillboxCharacter::EnableWalk()
{
	WalkEnabled = true;
	GEngine->AddOnScreenDebugMessage(-1, 0.5, FColor::Blue, "WALK Enable");
	ChangeMovementState();
}

void ATDS_SkillboxCharacter::DisableWalk()
{
	WalkEnabled = false;
	GEngine->AddOnScreenDebugMessage(-1, 0.5, FColor::Blue, "WALK Disable");
	ChangeMovementState();
}

void ATDS_SkillboxCharacter::ShootAction()
{
	CharAttackEvent(true);
	
	/*if(AimEnabled)
	{
		PlayAnimMontage(ShootAimed, 1, NAME_None);
		
	}
	else
	{
		PlayAnimMontage(ShootUnAimed, 1, NAME_None);
	}*/
}

void ATDS_SkillboxCharacter::StopShootAction()
{
	CharAttackEvent(false);
}

void ATDS_SkillboxCharacter::CharAttackEvent(bool bIsFiring)
{
	ADefaultWeapon* WeaponInHands = nullptr;
	WeaponInHands = GetCurrentWeapon();
	if (WeaponInHands)
	{
		//ToDo Check melee or range
		WeaponInHands->SetWeaponStateFire(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

void ATDS_SkillboxCharacter::ReloadAction()
{
/*	if(CurrentWeapon)
	{
		if (CurrentWeapon->GetWeaponRound() <= CurrentWeapon->WeaponSetting.MaxRound)
			CurrentWeapon->InitReload();
	}
*/
} 

void ATDS_SkillboxCharacter::MovementTick()
{
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

	CharIsMoving = (((AxisX > 0.2f) || (AxisX < -0.2f)) || ((AxisY > 0.2f) || (AxisY < -0.2f)));

	APlayerController* CurrentPlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);	

	if((MovementState != EMovementState::SprintRun_State))
	{
		GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
		FHitResult HitResult;
		CurrentPlayerController->GetHitResultUnderCursor(ECC_GameTraceChannel1, false, HitResult);
		float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location).Yaw;
		SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));
	}
	else if ((MovementState != EMovementState::SprintRun_State) && CharIsMoving)
	{
		GetCharacterMovement()->bOrientRotationToMovement = false; // Not rotate character to moving direction
	}

}

void ATDS_SkillboxCharacter::MouseOnDisplayTick(float DeltaSeconds)
{

	if(AimEnabled)
	{
		//Get controlelr
		APlayerController* CurrentPlayerControllerForMouse = UGameplayStatics::GetPlayerController(GetWorld(), 0);

		//Get viewport size
		FVector2D ViewportSize = FVector2D(GEngine->GameViewport->Viewport->GetSizeXY());

		//Get mouse position on screen
		float MouseX;
		float MouseY;
		CurrentPlayerControllerForMouse->GetMousePosition(MouseX, MouseY);

		//CameraBoom current and target lcoations
		FVector CameraBoomCurrentLocation = CameraBoom->GetComponentLocation();
		FVector CameraBoomTargetLocation;

		//Set maximum camera offset in relation to player
		float XmaxOffset = MouseX - (ViewportSize.X / 2);
		float YmaxOffset = MouseY - (ViewportSize.Y / 2);

		//Set Alpha for interpolating camera offset from player depending of mouse offset from display center
		float LerpXpos = UKismetMathLibrary::MapRangeClamped
			(UKismetMathLibrary::Abs(MouseX - ViewportSize.X / 2),
			ViewportSize.X / 2 / 2,
			ViewportSize.X / 2,
			0,
			1);
		
		float LerpYpos = UKismetMathLibrary::MapRangeClamped
			(UKismetMathLibrary::Abs(MouseY - ViewportSize.Y / 2),
			ViewportSize.Y / 2 / 2,
			ViewportSize.Y / 2,
			0,
			1);

		//Calculating CameraBoom target position
		CameraBoomTargetLocation = GetActorLocation() + UKismetMathLibrary::MakeVector
			(UKismetMathLibrary::Lerp(0.0f, YmaxOffset, LerpYpos) * -1.0f,
			UKismetMathLibrary::Lerp(0.0f, XmaxOffset, LerpXpos) / 2.0f,
			GetActorLocation().Z);

		//Setting smooth camera offset
		FVector AimEnabledCameraOffset = UKismetMathLibrary::VInterpTo(CameraBoomCurrentLocation, CameraBoomTargetLocation, DeltaSeconds, 5.0f);

		//Setting CameraBoom position
		CameraBoom->SetWorldLocation(AimEnabledCameraOffset);
	}

	//Setting default position if Aim disabled
	else
	{
		FVector CameraBoomCurrentLocation = CameraBoom->GetComponentLocation();
		CameraBoom->SetWorldLocation(UKismetMathLibrary::VInterpTo(CameraBoomCurrentLocation, GetActorLocation(), DeltaSeconds, 5.0f));
	}

	//debug
	//FString VectorAsString = FString::Printf(TEXT("X: %f, Y: %f, Z: %f"), CameraBoomTargetLocation.X, CameraBoomTargetLocation.Y, CameraBoomTargetLocation.Z);
	//GEngine->AddOnScreenDebugMessage(-1, 0.5, FColor::Red, VectorAsString);
	
}

void ATDS_SkillboxCharacter::CharacterUpdate()
{
	float ResultSpeed = 400.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResultSpeed = MovementInfo.AimSpeedNormal;
		break;
	case EMovementState::AimWalk_State:
		ResultSpeed = MovementInfo.AimSpeedWalk;
		break;
	case EMovementState::Walk_State:
		ResultSpeed = MovementInfo.WalkSpeedNormal;
		break;
	case EMovementState::Run_State:
		ResultSpeed = MovementInfo.RunSpeedNormal;
		break;
	case EMovementState::SprintRun_State:
		ResultSpeed = MovementInfo.SprintRunSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResultSpeed;
	
}

void ATDS_SkillboxCharacter::ChangeMovementState()
{
	if (!WalkEnabled && (!bInputSprint || !CanSprint) && !AimEnabled)
	{
		MovementState = EMovementState::Run_State;
	}
	else
	{
		if (bInputSprint && CanSprint)
		{
			WalkEnabled = false;
			AimEnabled = false;
			MovementState = EMovementState::SprintRun_State;
		}
		if (WalkEnabled && !bInputSprint && AimEnabled)
		{
			MovementState = EMovementState::AimWalk_State;
		}
		else
		{
			if (WalkEnabled && !bInputSprint && !AimEnabled)
			{
				MovementState = EMovementState::Walk_State;
			}
			else
			{
				if (!WalkEnabled && !bInputSprint && AimEnabled)
				{
					MovementState = EMovementState::Aim_State;
				}
			}
		}
	}
	CharacterUpdate();
}

ADefaultWeapon* ATDS_SkillboxCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATDS_SkillboxCharacter::InitWeapon()
{
	if(WeaponToInit)
	{
		FVector SpawnLocation = FVector(0);
		FRotator SpawnRotation = FRotator(0);

		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParams.Owner = GetOwner();
		SpawnParams.Instigator = GetInstigator();

		ADefaultWeapon* SpawnedWeapon = Cast<ADefaultWeapon>(GetWorld()->SpawnActor(WeaponToInit, &SpawnLocation, &SpawnRotation, SpawnParams));

		if(SpawnedWeapon)
		{
			FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
			SpawnedWeapon->AttachToComponent(GetMesh(), Rule, FName ("RightHand_WeaponSocket"));
			CurrentWeapon = SpawnedWeapon;
		}
	}
}

//"RightHand_WeaponSocket"