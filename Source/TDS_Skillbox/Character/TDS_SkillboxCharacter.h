// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDS_Skillbox/FunctionLibrary/GameTypesClass.h"
#include "TDS_Skillbox/GameObjects/DefaultWeapon.h"

#include "TDS_SkillboxCharacter.generated.h"


UCLASS(Blueprintable)
class ATDS_SkillboxCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATDS_SkillboxCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent) override;


	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	//FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	//class UDecalComponent* CursorToWorld;

protected:

	virtual void BeginPlay() override;

	
public:
		
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement | States")
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement | States")
		FCharacterSpeed MovementInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement | States")
		bool bInputSprint = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement | States")
		bool WalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement | States")
		bool AimEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement | States")
		bool CharIsMoving;

	//Input movement functions
	UFUNCTION()
	void InputAxisX(float Value);
	UFUNCTION()
	void InputAxisY(float Value);
	UFUNCTION()
	void InputActionStartSprint();
	UFUNCTION()
	void InputActionStopSprint();

	//Input movement axis
	float AxisX = 0.0f;
	float AxisY = 0.0f;

	// Stamina values
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement | Stamina")
	int StaminaCurrent = 100;

	int StaminaMax = 100;
	int StaminaMin = 0;
	float StaminaConsRate = 0.05;
	float StaminaResRate = 0.025;
	float StaminaResDelay = 1;
	FTimerHandle StaminaResTimer;
	FTimerHandle StaminaConsTimer;
	bool CanSprint;

	//Input action functions
	UFUNCTION()
		void StaminaConsumption();
	UFUNCTION()
		void StaminaRestoration();
	UFUNCTION()
		void EnableAim();
	UFUNCTION()
		void DisableAim();
	UFUNCTION()
		void EnableWalk();
	UFUNCTION()
		void DisableWalk();
	UFUNCTION()
		void ShootAction();
	UFUNCTION()
		void StopShootAction();
	UFUNCTION()
		void CharAttackEvent(bool bIsFiring);
	UFUNCTION()
		void ReloadAction();

	//Movement tick function
	UFUNCTION()
		void MovementTick();

	//Mouse detecting function
	UFUNCTION()
		void MouseOnDisplayTick(float DeltaSeconds);

	//Movement states update functions
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();
	
	//Animations values
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement | Animation")
	UAnimMontage* ShootAimed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement | Animation")
	UAnimMontage* ShootUnAimed;

	//Weapon init
	ADefaultWeapon* CurrentWeapon = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Temporary open type")
	TSubclassOf<ADefaultWeapon> WeaponToInit = nullptr;

	UFUNCTION()
	ADefaultWeapon* GetCurrentWeapon();
	UFUNCTION()
	void InitWeapon();

	
};