// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_SkillboxPlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "TDS_Skillbox/Character/TDS_SkillboxCharacter.h"
#include "Engine/World.h"

ATDS_SkillboxPlayerController::ATDS_SkillboxPlayerController()
{
	bShowMouseCursor = true;
	//DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void ATDS_SkillboxPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
}

void ATDS_SkillboxPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();
	
}