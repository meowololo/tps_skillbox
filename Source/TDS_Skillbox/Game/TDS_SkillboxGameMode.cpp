// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_SkillboxGameMode.h"
#include "TDS_SkillboxPlayerController.h"
#include "TDS_Skillbox/Character/TDS_SkillboxCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATDS_SkillboxGameMode::ATDS_SkillboxGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDS_SkillboxPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}