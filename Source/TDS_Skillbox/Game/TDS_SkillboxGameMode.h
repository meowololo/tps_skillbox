// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDS_SkillboxGameMode.generated.h"

UCLASS(minimalapi)
class ATDS_SkillboxGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDS_SkillboxGameMode();
};



