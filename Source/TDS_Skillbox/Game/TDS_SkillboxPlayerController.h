// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TDS_SkillboxPlayerController.generated.h"

UCLASS()
class ATDS_SkillboxPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	
	ATDS_SkillboxPlayerController();

protected:

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface
};


