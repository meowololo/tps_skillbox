// Fill out your copyright notice in the Description page of Project Settings.


#include "DefaultWeapon.h"
#include "NavigationSystemTypes.h"
#include "GameFramework/ProjectileMovementComponent.h"

// Sets default values
ADefaultWeapon::ADefaultWeapon()
{

	PrimaryActorTick.bCanEverTick = true;
	
	SceneComponent = CreateDefaultSubobject<USceneComponent>("Scene");
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>("SkeletalMesh");
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName("NoCollision");
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName("NoCollision");
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootVector = CreateDefaultSubobject<UArrowComponent>("ShootVector");
	ShootVector->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ADefaultWeapon::BeginPlay()
{
	Super::BeginPlay();
	
	WeaponInit();
	
}

// Called every frame
void ADefaultWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
}

void ADefaultWeapon::WeaponInit()
{
	if(SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent(true);
	}
}

bool ADefaultWeapon::WeaponCanFire()
{
	return true;
}

FProjectileInfo ADefaultWeapon::GetProjectile()
{
	return WeaponSettings.ProjectileSettings;
}

void ADefaultWeapon::SetWeaponStateFire(bool bIsFire)
{
	if (WeaponCanFire())
	{
		WeaponFiring = bIsFire;
	}
	else
	{
		WeaponFiring = false;
	}
	
	//Debug
	//FString DebugMessage = FString::Printf(TEXT("MyBool Value: %s"), WeaponFiring ? TEXT("true") : TEXT("false"));
	//GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::White, DebugMessage);
}


void ADefaultWeapon::FireTick(float DeltaTime)
{
	//debug
	FString VectorAsString = FString::Printf(TEXT("X: %f"), FireTime);
	GEngine->AddOnScreenDebugMessage(-1, 0.5, FColor::Green, VectorAsString);
	
	if (WeaponFiring)
	{
		if (FireTime < 0.f)
		{
			Fire();
		}
		else
		{
			FireTime -= DeltaTime;
		}
	}
}

void ADefaultWeapon::Fire()
{
	FireTime = WeaponSettings.RateOfFire;
	GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::White, "Fire");
	
	if (ShootVector)
	{
		FVector SpawnLocation = ShootVector->GetComponentLocation();
		FRotator SpawnRotation = ShootVector->GetComponentRotation();
		FProjectileInfo ProjectileInfo = GetProjectile();

		if (ProjectileInfo.Projectile)
		{
			//Projectile Init ballistic fire

			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParams.Owner = GetOwner();
			SpawnParams.Instigator = GetInstigator();

			ADefaultProjectile* ProjectileToShoot = Cast<ADefaultProjectile>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
			if (ProjectileToShoot)
			{
				//ToDo Init Projectile settings by id in table row(or keep in weapon table)
				ProjectileToShoot->InitialLifeSpan = 10.0f;
				ProjectileToShoot->BulletProjectileMovement->InitialSpeed = 2500.0f;
			}
			
		}

		else
		{
			//ToDo Projectile null Init trace fire
		}
		
	}
}


void ADefaultWeapon::UpdateStateWeapon(EMovementState NewMovementState)
{
	//ToDo Dispersion
	ChangeDispersion();
}

void ADefaultWeapon::ChangeDispersion()
{
}