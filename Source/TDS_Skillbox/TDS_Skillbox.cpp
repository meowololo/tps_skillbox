// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_Skillbox.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS_Skillbox, "TDS_Skillbox" );

DEFINE_LOG_CATEGORY(LogTDS_Skillbox)
 